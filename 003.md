# Manipulating container

## Objectives

- Modify a running container.
- Multiple ports.

## Docker objectives

- Docker port mapping
- Attach into a container

### TODO

Run a container with:

- Image: `dockersamples/static-site`
- Port: `Random`

Attach your terminal to the container in execution:

`docker container attach $CONTAINER_ID`

Try to access the container using the random port generated in a browser or:

```shell
curl http://127.0.0.1:$RANDOM_PORT
```

### Modifying the container in execution

- Install an editor like `vim` in the container in execution.
- Modify the file `index.html`.

The result expected of the webpage should look like:

![img](./sample.png)

- Replace with your name.